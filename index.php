<?php
    require("DataAccess.php");
    //Se crea el objeto para realizar consultas o usar funciones con la base datos
    $database = new DataAccess();
?> 


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
     integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Rally</title>
</head>
<body>
    
    <div class="container">
        <h2>Formulario Estudiante</h2>
        <form action="StudentForm.php" method="post">
            <div class="form-group">
            <label for="Identification">Cedula</label>
            <input type="text" name="identification" id="Identification" class="form-control" placeholder="">
            </div>
            <div class="form-group">
            <label for="Name">Nombre</label>
            <input type="text" name="name" id="Name" class="form-control" placeholder="">
            </div>
            <div class="form-group">
            <label for="Lastname">Apellidos</label>
            <input type="text" name="lastname" id="Lastname" class="form-control" placeholder="">
            </div>
            <div class="form-group">
            <label for="Major">Carrera</label>
            <select class="form-control" name="major" id="Major">
                    <?=
                        //Aqui se agregara lo que imprima la funcion selectMajors en este caso las carreras
                        $database->selectMajors();
                    ?>                
            </select>
            </div>
            <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
    </div>
    



    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
     integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>